/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   main.cpp
 * Author: elias
 *
 * Created on 25 de febrero de 2016, 15:13
 */

#include <QObject>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include  <unistd.h>

#include <iostream>
#include <QApplication>
#include "src/GUI/VentanaPrincipal.h"
#include "src/Universo/Manager.h"
#include "src/Comunicacion/Servidor.hpp"

using namespace std;
using std::cout;

VentanaPrincipal *_ventanaServer;
int _quantumlvl1 = 1;
int _quantumlvl2 = 10;
int _quantumlvl3 = 100;

QApplication *app;

void *thread_funcion01(void *arg){
    while(true){
        
        //app->exec();
        //usleep(1);
        
    }
    return NULL;
}

void *thread_funcion02(void *arg){
    while(true){
        
        printf("actualización de universo.\n");
        usleep(1);
        
    }
    return NULL;
}

void *thread_funcion03(void *arg){
    while(true){
        printf("EnviarMensaje.\n");
        Manager::CargarManager()->enviarMensaje();
        usleep(1);
        
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    
    app = new QApplication(argc, argv);
   
    
    Manager::CargarManager()->rellenarBloques();
    //Manager::CargarManager()->instanciarBola();
    pthread_t _GUI ,_Tiempo, _Mensaje;
   
    _ventanaServer = new VentanaPrincipal(app);
    Manager::CargarManager()->enlazarVentana(_ventanaServer);
    Manager::CargarManager()->enlazarGUI(app);
    
//    if (pthread_create( &_GUI, NULL, thread_funcion01, NULL) ) {
//        printf("Error creando el hilo de recibir.");
//        abort();
//    }
    
    if (pthread_create( &_Tiempo, NULL, thread_funcion02, NULL) ) {
        printf("Error creando el hilo de De Universo");
        abort();
    }
    
    if (pthread_create( &_Mensaje, NULL, thread_funcion03, NULL) ) {
        printf("Error creando el hilo de Envio.");
        abort();
    }   
        
    app->exec();
    usleep(1);
        
    
        pthread_join(_Tiempo, NULL);
        pthread_join(_Mensaje, NULL);
    
        return app->exec();
    
}
