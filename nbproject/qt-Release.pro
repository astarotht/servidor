# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Release/GNU-Linux-x86
TARGET = servidor
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += release 
PKGCONFIG +=
QT = core gui widgets
SOURCES += main.cpp src/Comunicacion/Servidor.cpp src/Estructuras/Lista.cpp src/Estructuras/Nodo.cpp src/GUI/VentanaPrincipal.cpp src/Universo/Barra.cpp src/Universo/Bloques.cpp src/Universo/Bola.cpp src/Universo/CBFabricaDeComponentes.cpp src/Universo/CicloJuego.cpp src/Universo/Manager.cpp
HEADERS += src/Comunicacion/Servidor.hpp src/Estructuras/Lista.hpp src/Estructuras/Nodo.hpp src/GUI/VentanaPrincipal.h src/Universo/Barra.h src/Universo/Bloques.h src/Universo/Bola.h src/Universo/CBFabricaDeComponentes.h src/Universo/CicloJuego.h src/Universo/Manager.h
FORMS +=
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Release/GNU-Linux-x86
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += 
LIBS += 
