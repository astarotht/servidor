/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bola.cpp
 * Author: elias
 * 
 * Created on 29 de febrero de 2016, 17:04
 */

#include "Bola.h"


Bola::Bola(int pPosX, int pPosY){
    
    _posX = pPosX;
    _posY = pPosY;
    _velocidad = 5;
    _angulo = 90;

}

void Bola::mover(){
    
    
    switch (_angulo){
        
    case 45:
        _posX + _velocidad;
        _posY - _velocidad;
        break;
    
    case 90:
        _posY - _velocidad;
        break;
    
    case 135:
        _posX - _velocidad;
        _posY - _velocidad;
        break;
        
    case 225:
        _posX - _velocidad;
        _posY + _velocidad;
        break;
    
    case 270:
        _posY + _velocidad;
        break;
        
    case 315:
        _posX + _velocidad;
        _posY + _velocidad;
        break;
    default :
        break;
        
    }

}

/**
 * @brief Se utiliza par obtener la posición en x
 * @return  retorna el valor de x
 */
int Bola::get_posX(){
    
    return _posX;
    

}
/**
 * @brief Se utiliza para obtener la posición de la y de la bola
 * @return  retorna el valor de y
 */
int Bola::get_posY(){
    
    return _posY;

}
//int get_posX();
//int get_posY();

Bola::Bola(const Bola& orig) {
}

Bola::~Bola() {
}

