/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bloques.h
 * Author: elias
 *
 * Created on 28 de febrero de 2016, 17:54
 */

#ifndef BLOQUES_H
#define BLOQUES_H


class Bloques {
public:
    
    Bloques();
    Bloques(int, int, int, int);
    int get_LvlDeBloque();
    int get_posX();
    int get_posFX();
    int get_posY();
    int get_posFY();
    int get_hp();
    void setPosX(int);
    void setPosY(int);
    void anchoDelBloque(int);
    bool colision();
    
    virtual ~Bloques();
private:
    bool _destruido;
    int _LvlDeBloque;
    int _posX;
    int _posFX;
    int _posY;
    int _posFY;
    int _hp;
    int _ancho;

};

#endif /* BLOQUES_H */