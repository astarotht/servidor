/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Manager.h
 * Author: elias
 *
 * Created on 25 de febrero de 2016, 22:11
 */

#ifndef MANAGER_H
#define MANAGER_H
#include <string>
#include <sstream>
#include "../Estructuras/Lista.hpp"
#include "../Comunicacion/Servidor.hpp"
#include "../GUI/VentanaPrincipal.h"
#include "../Universo/Bola.h"
#include "Bloques.h"
#include <QObject>
#include <QApplication>


using std::string;

class Manager
{
public:

    Servidor* testingGetServidor();

    static Manager* CargarManager()
    {

        if (_ManagerInstancia == NULL)
        {

            _ManagerInstancia = new Manager();

        }
        return _ManagerInstancia;

    }


    void despertarServidor(int, int, string);
    void enlazarVentana(VentanaPrincipal*);
    void enlazarGUI(QApplication*);
    void cambiarFondoDeVentana();
    void rellenarBloques();
    void instanciarBola();
    Lista<Bola*>* obtenerListaDeBolas();
    void enviarMensaje();
    string recibirMensaje(int);

private:
    Manager();
    string numeroaString(int);

    int _anguloActual;
    static Manager *_ManagerInstancia;
    const char *_bufferout;
    string _bufferin;
    
    Lista<int> *_mensajesRecibido;
    Lista<string> *_mensajesRecibidos;
    Lista<string> *_mensajesaenviar;

    Lista<Bola*> *_bolas;
    Bloques *_Matrizbloques[80][10];
    bool _banderaServidor;
    Servidor *_servidor;
    VentanaPrincipal *_Ventana;
    QApplication *_programa;
    string _mensaje;

};

#endif /* MANAGER_H */