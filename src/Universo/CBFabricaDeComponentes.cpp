/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CBFabricaDeComponentes.cpp
 * Author: elias
 * 
 * Created on 28 de febrero de 2016, 19:02
 */

#include "CBFabricaDeComponentes.h"

CBFabricaDeComponentes* CBFabricaDeComponentes::_Fabrica = 0;

CBFabricaDeComponentes::CBFabricaDeComponentes() {
    
}

/**
 * @brief Se utiliza para obtener un bloque
 * @param pPosX posición en x del bloque
 * @param pPosY posición en y del bloque
 * @param pTipo tipo de bloque
 * @param pancho ancho que va a poseer el bloque
 * @return retorna el puntero del nuevo bloque creado
 */
Bloques* CBFabricaDeComponentes::crearBloque(int pPosX, int pPosY, int pTipo, int pancho = 10){
    
    return new Bloques( pPosX, pPosY, pTipo, pancho);

}
/**
 * @brief Se utiliza para obtener una bola instanciada
 * @param pPosX posición en x de la bola
 * @param pPosY posición en y de la bola
 * @param angulo angulo en que se va a desplazar la bola 
 * @return retorna el puntero que indica donde se encuentra la instancia de la bola
 */
Bola* CBFabricaDeComponentes::crearBola(int pPosX, int pPosY){
    
    return new Bola(pPosX, pPosY);

}

CBFabricaDeComponentes::CBFabricaDeComponentes(const CBFabricaDeComponentes& orig) {
}

CBFabricaDeComponentes::~CBFabricaDeComponentes() {
}

