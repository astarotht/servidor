/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bola.h
 * Author: elias
 *
 * Created on 29 de febrero de 2016, 17:04
 */

#ifndef BOLA_H
#define BOLA_H

class Bola {
public:
    Bola();
    Bola(int pPosX, int pPosY);
    int get_posX();
    int get_posY();
    void aumentarVelocidad();
    void mover();
    
    Bola(const Bola& orig);
    virtual ~Bola();
    
private:
    
    int _posX;
    int _posY;
    int _velocidad;
    int _angulo;

};

#endif /* BOLA_H */

