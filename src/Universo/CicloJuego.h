/* 
 * File:   CicloJuego.h
 * Author: andres
 *
 * Created on 2 de marzo de 2016, 21:20
 */

#ifndef CICLOJUEGO_H
#define	CICLOJUEGO_H
#include "Bloques.h"
#include "Bola.h"
#include <iostream>

class CicloJuego {
public:
    CicloJuego();
    CicloJuego(Bola [], Bloques[]);
    CicloJuego(const CicloJuego& orig);
    virtual ~CicloJuego();
private:
    Bloques ListBloques[200];
    Bola ListBolas[10];
    
    
};

#endif	/* CICLOJUEGO_H */

