/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CBFabricaDeComponentes.h
 * Author: elias
 *
 * Created on 28 de febrero de 2016, 19:02
 */

#ifndef CBFABRICADECOMPONENTES_H
#define CBFABRICADECOMPONENTES_H
#include "Bola.h"
#include "Bloques.h"

class CBFabricaDeComponentes {
public:
    CBFabricaDeComponentes();
    
    static CBFabricaDeComponentes* acceso(){
        
        if (_Fabrica == 0){
            
            _Fabrica = new CBFabricaDeComponentes();
        
        }
        return _Fabrica;
    
    };
    
    Bloques* crearBloque(int, int, int, int);
    Bola* crearBola(int, int);
    CBFabricaDeComponentes(const CBFabricaDeComponentes& orig);
    virtual ~CBFabricaDeComponentes();
private:
    static CBFabricaDeComponentes* _Fabrica;

};

#endif /* CBFABRICADECOMPONENTES_H */

