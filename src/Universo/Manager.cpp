/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Manager.cpp
 * Author: elias
 * 
 * Created on 25 de febrero de 2016, 22:11
 */

#include "Manager.h"
#include "CBFabricaDeComponentes.h"

Manager* Manager::_ManagerInstancia = NULL;

Manager::Manager()
{ 
    
    _anguloActual = 0;
    _bolas = new Lista<Bola*>();
    _banderaServidor = false;

}
/**
 * @brief Abre el puerto para que se puedan conectar clientes de juego al servidor.
 * @param pCantidadDeJugadores indica la cantidad de jugadores que se tienen que conectar
 * @param ppuerto indica el puerto a usar en la conexión.
 * @param pip indica la ip que el servidor va a utilizar
 */
void Manager::despertarServidor(int pCantidadDeJugadores, int ppuerto, string pip)
{

    if (!_banderaServidor)
    {
        
        _mensajesRecibidos = new Lista<string>();
        _mensajesaenviar =  new Lista<string>();
        
        _servidor = new Servidor(ppuerto, pip.c_str(), 1024);
        _servidor->establecerConexionSocket();
        _servidor->enlazarServidor();
        _servidor->escucharConexiones(pCantidadDeJugadores);
        _servidor->aceptarConexiones();
        cambiarFondoDeVentana();
        _banderaServidor = true;
    }
}
/**
 * @brief Se indica cual la dirección en memoria de la GUI del servidor
 * @param ptrventana El puntero que indica la posición de la ventana
 */
void Manager::enlazarVentana(VentanaPrincipal* ptrventana)
{

    _Ventana = ptrventana;

}
/**
 * @brief Se realiza pra enlazar qt con el manager. 
 * @param pPrograma Dirección de la app qt creada
 */
void Manager::enlazarGUI(QApplication* pPrograma)
{

    _programa = pPrograma;

}

/**
 *@brief Se cambia de color el fondo de la ventana
 */
void Manager::cambiarFondoDeVentana()
{

    //myWidget->setStyleSheet("background-color:black;");
    _Ventana->setStyleSheet("background-color:blue");

}

/**
 *@brief Se rellena la matriz de bloques 
 */
void Manager::rellenarBloques(){
    
    for(int j = 0; j < 10;j++){
        
        for(int i = 0; i < 80; i++){
        
        _Matrizbloques[i][j] = CBFabricaDeComponentes::acceso()->crearBloque(i*10, j*5, 0,10);
   
        }
    }
    
}
//
///**
// * @brief se instancia una bola en el arreglo de las bolas.
// */
//void Manager::instanciarBola(){
//    
//    if (_anguloActual == 315){
//        
//        _anguloActual += 45;
//    
//    }else{
//        
//        _anguloActual = 0;
//    
//    }
//    
//    _bolas->insertarFinal(CBFabricaDeComponentes::acceso()->crearBola(400, 400));
//
//}


/**
 * @brief brinda un acceso al arreglo en donde se encuentra las bolas de breakout
 * @return acceso a la estructura donde se encuentran las bola
 */
Lista<Bola*>* Manager::obtenerListaDeBolas(){
    
    return _bolas;

}

/**
 * @brief brinda acceso al socket del servidor se udebe utilizar sólo para probar conexiones
 * @return retorna el socket servidor
 */
Servidor* Manager::testingGetServidor()
{

    return _servidor;

}
/**
 * @brief Se recibe los mensajes enviados por el cliente
 * @param pCliente
 * @return 
 */
string Manager::recibirMensaje(int pCliente){

    return "";
    
}
/**
 * @brief Metodo utilizado para enviar el estado actual del universo
 */
void Manager::enviarMensaje(){
  
    
    string temporal;
    _mensaje = "Draw|";
    if(_bolas->obtenerLargo() != 0 && _banderaServidor){
    _mensaje +="*bola#" + numeroaString(0) + "#" 
    + numeroaString(_bolas->obtenerPosicion(0)->get_posX()) + "#" +
    numeroaString(_bolas->obtenerPosicion(0)->get_posY()) + "*";
    _mensaje += "|end";
            
    _servidor->enviarDatos(_mensaje);
    }

}
/**
 * @brief Se utiliza para convertir un dato tipo int a string
 * @param pNumero datos de tipo int que se convierte a string
 * @return  retorna un string 
 */
string Manager::numeroaString(int pNumero){
    std::ostringstream $string;
    $string << pNumero;
    return $string.str(); 

    

}
