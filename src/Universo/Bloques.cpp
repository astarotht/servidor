/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bloques.cpp
 * Author: elias
 * 
 * Created on 28 de febrero de 2016, 17:54
 */

#include "Bloques.h"

Bloques::Bloques(int pPosX, int pPosY, int pTipo, int pAncho) {
    
    _LvlDeBloque = pTipo;
    _posX = pPosX;
    _posY = pPosY;
    _posFX = pPosX + pAncho;
    _posFY = pPosY + pAncho/2;
    
}

void Bloques::anchoDelBloque(int pancho){
    
    _ancho = pancho;

}

bool Bloques::colision(){
    
    return false;

}

int Bloques::get_LvlDeBloque(){
    
    return _LvlDeBloque;

}
int Bloques::get_hp(){
    
    return _hp;

}

int Bloques::get_posX(){
    
    return _posX;

}

int Bloques::get_posFX(){
    
    return _posFX;

}

int Bloques::get_posY(){
    
    return _posY;

}

int Bloques::get_posFY(){
    
    return _posFY;

}

void Bloques::setPosY(int pPosY){
    
    _posY = pPosY;
    _posFY = pPosY + _ancho;

}

void Bloques::setPosX(int pPosX){
    
    _posX = pPosX;
    _posFX = pPosX + _ancho;

}

Bloques::~Bloques() {
}

