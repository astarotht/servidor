/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VentanaPrincipal.h
 * Author: elias
 *
 * Created on 25 de febrero de 2016, 15:52
 */

#ifndef VENTANAPRINCIPAL_H
#define VENTANAPRINCIPAL_H

#include <QApplication>
#include <QObject>
#include <QPushButton>
#include <QFont>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <string>

using std::string;

class VentanaPrincipal : public QWidget
{
    Q_OBJECT

public:
    VentanaPrincipal(QApplication*);
    bool getBanderaDeDatos();
    string getIp();
    int getPuerto();

public slots:
    void LevantarBanderaDeDatosLlenos();
signals:
    bool _sePuedeCrearelServidor(int);

private:
    QApplication* _Programa;
    QLabel* _Titulo1;
    QLabel* _LabelPuerto;
    QLineEdit* _NumeroDePuerto;
    QLabel* _LabelServerIp;
    QLineEdit* _NumeroDeIp;
    QPushButton* _BotonDeCargar;
    string _ip;
    int _puerto;

    bool _DatosLlenos;

};

#endif /* VENTANAPRINCIPAL_H */

