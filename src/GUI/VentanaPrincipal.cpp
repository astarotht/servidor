/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VentanaPrincipal.cpp
 * Author: elias
 * 
 * Created on 25 de febrero de 2016, 15:52
 */

#include "VentanaPrincipal.h"
#include "../Universo/Manager.h"
#include <QApplication>

VentanaPrincipal::VentanaPrincipal(QApplication* pprograma) : QWidget()
{
    _Programa = pprograma;
    //this->windowTitleChanged("Servidor Marbas");
    _DatosLlenos = false;
    _Titulo1 = new QLabel("Inserte los datos, requeridos para iniciar el servidor", this);
    _LabelPuerto = new QLabel("Inserte el numero de puerto aquí", this);
    _NumeroDePuerto = new QLineEdit(this);
    _LabelServerIp = new QLabel("Inserte el numero de ip que usará el servidor", this);
    _NumeroDeIp = new QLineEdit(this);
    _BotonDeCargar = new QPushButton("IniciarServidor", this);
    
    this->resize(400, 200);
    _Titulo1->move(10, 10);
    _LabelPuerto->move(10, 50);
    _NumeroDePuerto->move(10, 80);
    _LabelServerIp->move(10, 110);
    _NumeroDeIp->move(10, 140);
    _BotonDeCargar->move(150, 170);
    QObject::connect(this->_BotonDeCargar, SIGNAL(clicked()), this, SLOT(LevantarBanderaDeDatosLlenos()));

    this->show();

}

void VentanaPrincipal::LevantarBanderaDeDatosLlenos()
{

    _DatosLlenos = true;
    _Titulo1->setText("El servidor se ha iniciado, para empezar presione i en el cliente");
    _LabelPuerto->setText("El puerto seleccionado es " + _NumeroDePuerto->displayText());
    _LabelServerIp->setText("La ip a la que conectarse es " + _NumeroDeIp->displayText());
    _NumeroDePuerto->setEnabled(false);
    _NumeroDeIp->setEnabled(false);
    _BotonDeCargar->setEnabled(false);
    _ip = _NumeroDeIp->displayText().toStdString();
    _puerto = _NumeroDePuerto->displayText().toInt();

    emit _sePuedeCrearelServidor(0);
    this->repaint();
    Manager::CargarManager()->despertarServidor(1, _puerto, _ip);


}

bool VentanaPrincipal::getBanderaDeDatos()
{
    return _DatosLlenos;
}

string VentanaPrincipal::getIp()
{

    return _ip;

}

int VentanaPrincipal::getPuerto()
{

    return _puerto;

}