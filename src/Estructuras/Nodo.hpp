/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 @file:   Nodo.hpp
 @author: elias
 *
 @date on 19 de febrero de 2016, 0:55
 * 
 @brief clase utilizada para envolver otras clases y generar estructuras apartir
 de esta
 */

#ifndef NODO_HPP
#define NODO_HPP

template<typename pclase>
class Nodo {
public:

    /**   
    @brief Contructor del nodo, se le debe pasar el dato que se desea implementar
    @param pdato Se introduce la data que se va a utilizar en una estructura dinamica
    más adelante*/
    Nodo(pclase pdato) {
        _data = pdato;
    }

    /**   
    @brief Se obtiene el dato en el nodo
    @return Se obtiene el dato guardado, retorna un objeto del tipo que se le
     especificó al nodo en su instanciación*/
    pclase getDato() {

        return _data;
    }

    /**   
    @brief Se carga o cambia un dato en el nodo
    @param pDato objeto que se quiere incluir en el nodo*/
    void setDato(pclase pDato) {

        _data = pDato;

    }

    /**
     * @brief Retorna la ubicación del nodo que le sigue a este
     * @return Dirección del nodo
     */
    Nodo* getSiguiente() {
        return _siguiente;
    }

    /**
     * Retorna la dirección del nodo que se encuentra antes que este
     * @return puntero del nodo direccion del nodo
     */
    Nodo* getAnterior() {
        return _anterior;
    }

    /**
     * @brief Se le indica cual Nodo se encuentra antes de este
     * @param pNodo Se añade el apuntador de un nodo
     */
    void setAnterior(Nodo* pNodo) {
        _anterior = pNodo;
    }

    /**
     * @Se indica cual nodo es el que se encuentra despues de este
     * @param pNodo puntero que indica cual nodo es el que continua
     */
    void setSiguiente(Nodo* pNodo) {
        _siguiente = pNodo;
    }

    virtual ~Nodo() {

    };
private:
    pclase _data;
    Nodo *_siguiente;
    Nodo *_anterior;

};

#endif /* NODO_HPP */


